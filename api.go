package Raffle

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/RPGPN/crius-utils/LiveCrius"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	"io/ioutil"
	"net/http"
	"time"
)

const (
	twitchAPIRoot = "https://api.twitch.tv/helix"
	glimeshAPI    = "https://glimesh.tv/api"
)

func getTwitchNameFromID(id string, ctx context.Context) (*string, error) {
	api := LiveCrius.GetAPIFuncs(ctx)
	cache := LiveCrius.GetSettings(ctx).GetRedisCache()

	cached, hit, err := api.CheckCache(cc.PlatformTwitch, "namecache", id, cache)
	if err != nil {
		return nil, err
	}
	if hit {
		return cached, nil
	}

	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/users?id=%s", twitchAPIRoot, id), nil)
	if err != nil {
		return nil, err
	}

	res, err := api.MakeTwitchReq(req)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	data := &struct {
		Data []struct {
			DisplayName string `json:"display_name"`
		}
	}{}

	err = json.Unmarshal(body, data)
	if err != nil {
		return nil, err
	}

	err = api.AddToCache(cc.PlatformTwitch, "namecache", id, data.Data[0].DisplayName, cache, time.Hour*72)
	if err != nil {
		return nil, err
	}

	return &data.Data[0].DisplayName, nil
}

func getGlimeshNameFromID(id int, ctx context.Context) (*string, error) {
	api := LiveCrius.GetAPIFuncs(ctx)
	cache := LiveCrius.GetSettings(ctx).GetRedisCache()

	cached, hit, err := api.CheckCache(cc.PlatformGlimesh, "namecache", id, cache)
	if err != nil {
		return nil, err
	}
	if hit {
		return cached, nil
	}

	const query = "query ($id: Int) {\n  user(id: $id) {\n    username\n  }\n}\n"

	bodydata, err := json.Marshal(&struct {
		Query     string         `json:"query"`
		Variables map[string]int `json:"variables"`
	}{
		Query: query,
		Variables: map[string]int{
			"id": id,
		},
	})
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, glimeshAPI, bytes.NewReader(bodydata))
	if err != nil {
		return nil, err
	}

	res, err := api.MakeGlimeshReq(req, false)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	data := &struct {
		Data struct {
			User struct {
				Username string
			}
		}
	}{}

	err = json.Unmarshal(body, data)
	if err != nil {
		return nil, err
	}

	err = api.AddToCache(cc.PlatformGlimesh, "namecache", id, data.Data.User.Username, cache, time.Hour*72)
	if err != nil {
		return nil, err
	}

	return &data.Data.User.Username, nil
}
