package Raffle

const tableCreateScripts = `CREATE TABLE IF NOT EXISTS raffle__raffles (
		raffle_id serial primary key,
		realm_id text not null,
		platform text not null,
		raffle_name text not null
	);
	
	CREATE TABLE IF NOT EXISTS raffle__raffle_entries (
		entry_id serial primary key,
		realm_id text not null,
		platform text not null,
		user_id text not null,
		raffle_id int references raffle__raffles
	);`

const userEntryQuery = `SELECT COUNT(*) FROM raffle__raffle_entries AS re
    JOIN raffle__raffles rr on rr.raffle_id = re.raffle_id
    WHERE re.platform=$1 AND re.user_id=$2 AND re.realm_id=$3 AND rr.raffle_name=$4;`

const enterUserQuery = `INSERT INTO raffle__raffle_entries (realm_id, platform, user_id, raffle_id) VALUES ($1, $2, $3, (
    SELECT raffle__raffles.raffle_id FROM raffle__raffles
    WHERE raffle__raffles.realm_id=$1 AND raffle__raffles.platform=$2 AND raffle_name=$4)
);`

const enteredUsersQuery = `SELECT user_id FROM raffle__raffle_entries AS re
JOIN raffle__raffles rr on re.raffle_id = rr.raffle_id
WHERE re.platform=$1 AND re.realm_id=$2 AND rr.raffle_name=$3;`

const removeRaffleEntries = `DELETE FROM raffle__raffle_entries WHERE raffle_id=(
SELECT rr.raffle_id FROM raffle__raffles AS rr WHERE raffle_name=$1 AND rr.realm_id=$2 AND rr.platform=$3
);`
