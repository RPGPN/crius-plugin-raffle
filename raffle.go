package Raffle

import (
	"context"
	"fmt"
	"github.com/gempir/go-twitch-irc/v2"
	"github.com/jmoiron/sqlx"
	CriusUtils "gitlab.com/RPGPN/crius-utils"
	"gitlab.com/RPGPN/crius-utils/LiveCrius"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	goleshchat "gitlab.com/ponkey364/golesh-chat"
	"math/rand"
	"strconv"
	"time"
)

var PluginInfo = &cc.PluginJSON{
	Name:               "Raffle",
	License:            "BSD-3-Clause",
	Creator:            "Ben <Ponkey364>",
	URL:                "https://gitlab.com/RPGPN/crius-plugin-raffle/",
	Description:        "Raffle is a crius plugin that allows viewers to enter and then have one person randomly selected.",
	SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
	Commands: []*cc.PJCommand{
		{
			Name:               "Start Raffle",
			Activator:          "startraffle",
			Help:               "Start a raffle. Usage: startraffle [name]",
			HandlerName:        "startraffle",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
		},
		{
			Name:               "Enter Raffle",
			Activator:          "enterraffle",
			Help:               "Enter a raffle (you can only enter once). Usage: enterraffle [name]",
			HandlerName:        "enterraffle",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
		},
		{
			Name:               "Draw Raffle",
			Activator:          "drawraffle",
			Help:               "Draw a raffle. Usage: drawraffle [name]",
			HandlerName:        "drawraffle",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
		},
	},
}

type raffle struct {
	db *sqlx.DB
}

func Setup(ctx context.Context) (map[string]cc.CommandHandler, error) {
	// get stuff we need from ctx
	s := LiveCrius.GetSettings(ctx)

	db := s.GetDB()

	// run the table creation script from db.go
	// create a transaction
	tx := db.MustBegin()

	// run the script
	_, err := tx.Exec(tableCreateScripts)
	if err != nil {
		return nil, err
	}

	// done
	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	r := &raffle{
		db: db,
	}

	return map[string]cc.CommandHandler{
		"startraffle": r.StartRaffle,
		"enterraffle": r.EnterRaffle,
		"drawraffle":  r.DrawRaffle,
	}, nil
}

func (r *raffle) StartRaffle(m *cc.MessageContext, args []string) error {
	if len(args) == 0 {
		m.Send("you must specify a name for this raffle.")
		return nil
	}

	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID
	switch platform {
	case cc.PlatformGlimesh:
		message := m.PlatformData.(*goleshchat.ChatMessage)

		if !LiveCrius.GlimeshHasPermission(message) {
			m.Send("Only the streamer can start a raffle.")
			return nil
		}
	case cc.PlatformTwitch:
		message := m.PlatformData.(*twitch.PrivateMessage)
		if !LiveCrius.TwitchHasPermission(message) {
			m.Send("Only the streamer or moderators can start a raffle.")
			return nil
		}
	}

	_, err := r.db.Exec("INSERT INTO raffle__raffles (realm_id, platform, raffle_name) VALUES ($1, $2, $3)",
		realmID, platform.ToString(), args[0])
	if err != nil {
		return err
	}

	m.Send(fmt.Sprintf("Raffle %s created.", args[0]))
	return nil
}

func (r *raffle) EnterRaffle(m *cc.MessageContext, args []string) error {
	if len(args) == 0 {
		m.Send("you must specify the name of a raffle to enter")
		return nil
	}

	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID
	userID := m.Author.ID

	// has the user entered?
	var count int
	err := r.db.Get(&count, userEntryQuery, platform.ToString(), userID, realmID, args[0])
	if err != nil {
		return err
	}

	if count != 0 {
		m.Send("you have already entered this raffle")
		return nil
	}

	_, err = r.db.Exec(enterUserQuery, realmID, platform.ToString(), userID, args[0])
	if err != nil {
		return err
	}

	m.Send(fmt.Sprintf("You entered raffle %s", args[0]))

	return nil
}

func (r *raffle) DrawRaffle(m *cc.MessageContext, args []string) error {
	if len(args) == 0 {
		m.Send("you must specify the raffle name")
		return nil
	}

	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID
	switch platform {
	case cc.PlatformGlimesh:
		message := m.PlatformData.(*goleshchat.ChatMessage)

		if !LiveCrius.GlimeshHasPermission(message) {
			m.Send("Only the streamer can draw a raffle.")
			return nil
		}
	case cc.PlatformTwitch:
		message := m.PlatformData.(*twitch.PrivateMessage)
		if !LiveCrius.TwitchHasPermission(message) {
			m.Send("Only the streamer or moderators can draw a raffle.")
			return nil
		}
	}

	enteredUsers := []string{}
	err := r.db.Select(&enteredUsers, enteredUsersQuery, platform.ToString(), realmID, args[0])
	if err != nil {
		return err
	}

	if len(enteredUsers) < 2 {
		m.Send("cannot draw a raffle with less than 2 entries.")
		return nil
	}

	// seed a generator
	rng := rand.New(rand.NewSource(time.Now().Unix()))
	chosen := rng.Intn(len(enteredUsers) - 1)
	chosenID := enteredUsers[chosen]

	var username string
	switch platform {
	case cc.PlatformGlimesh:
		{
			conv, err := strconv.Atoi(chosenID)
			if err != nil {
				return err
			}

			name, err := getGlimeshNameFromID(conv, m.Context)
			if err != nil {
				return err
			}
			username = *name
		}
	case cc.PlatformTwitch:
		{
			name, err := getTwitchNameFromID(chosenID, m.Context)
			if err != nil {
				return err
			}
			username = *name
		}
	}

	m.Send(fmt.Sprintf("Drew %s for raffle %s", username, args[0]))

	// remove raffle entries
	_, err = r.db.Exec(removeRaffleEntries, args[0], realmID, platform.ToString())
	if err != nil {
		return err
	}

	// remove the raffle itself
	_, err = r.db.Exec("DELETE FROM raffle__raffles WHERE platform=$1 AND realm_id=$2 AND raffle_name=$3",
		platform.ToString(), realmID, args[0])
	if err != nil {
		return err
	}

	return nil
}
